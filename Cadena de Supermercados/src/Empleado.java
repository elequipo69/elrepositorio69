public class Empleado {
	private String nombre; // Nombre y apellidos del cajero/a
	private String supermercado; // El supermercado donde trabaja
	private int sueldo; // El sueldo que percibe
	private int numeroCuenta; // Para ingresar sueldo;

	public Empleado(String nombre2, String supermercado2) {
		setNombre(nombre2);
		setSupermercado(supermercado2);
	}

	public void pagar() { // Se le ingresa en cuenta su sueldo
		// TODO metodo a implementar
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getSupermercado() {
		return supermercado;
	}

	public void setSupermercado(String supermercado) {
		this.supermercado = supermercado;
	}
	
	public void fichar(boolean entradaSalida) {
		System.out.println("Metiendo en base de datos");
	}
}
