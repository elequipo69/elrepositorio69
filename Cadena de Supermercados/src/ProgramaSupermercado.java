
public class ProgramaSupermercado {

	public static void main(String[] args) {
		System.out.println("Me voy a crear un Cajero");
		Cajero c1 = new Cajero("Paco", "Pacomer S.L.");
		System.out.println("Paco el cajero va a fichar\n");
		System.out.println("----------------");
		c1.fichar(false);
		System.out.println("----------------\n\n");
		System.out.println("Voy a crear un Reponedor");
		Reponedor r1 = new Reponedor("Juan", "Almacen de Calle Larios");
		System.out.println("Juan del almacen va a fichar\n");
		System.out.println("----------------");
		r1.fichar(false);
		System.out.println("----------------");
		
	}

}
