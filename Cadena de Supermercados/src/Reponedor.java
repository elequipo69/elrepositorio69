/**
 * @class Reponedor clase para controlar un reponedor
 * @author JC
 * @date 2018-04-17
 **/

public class Reponedor extends Empleado{
	private String almacen; // Almacen asociado al reponedor

	public Reponedor(String nombre, String almacen) {
		super(nombre, almacen);
		this.almacen = almacen;
	}
	
	public void introducirPedido(Pedido p) { // Se registra que este reponedor
												// ha hecho una reposición de
												// productos de un pedido desde
												// almacén a supermercado
		// TODO metodo a implementar
	}

	@Override
	public void fichar(boolean entradaSalida) {
		System.out.println("Picando tarjeta");
		super.fichar(entradaSalida);
	}
	
	
}
