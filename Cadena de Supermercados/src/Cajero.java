/**
 * @class Cajero clase para controlar un cajero
 * @author JC
 * @date 2018-04-17
 **/

public class Cajero extends Empleado{
	private int caja; //La caja donde trabaja el cajero
	
	public Cajero(String nombre, String supermercado) {
		super(nombre, supermercado);
	}
	
	public int introducirCompra(Compra c){ //Se registra que este cajero ha gestionado la compra de un cliente
		return 50; //TODO metodo a implementar
	}

	@Override
	public void fichar(boolean entradaSalida) {
		System.out.println("Introduciendo contraseņa");
		//Codigo para meter en base de datos la hora de entrada/salida
		super.fichar(entradaSalida);
	}
	
	
}