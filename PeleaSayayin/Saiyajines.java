package PeleaSayayin;
/*contructor padre*/
public class Saiyajines {
	protected int puntosDeVida;
	protected int puntosDeResistencia;
	protected int puntosAtaqueFisico;
	protected int puntosAtaqueKi;
	
	
	public Saiyajines(int puntosDeVida,int puntosDeResistencia,int puntosAtaqueFisico,int puntosAtaqueKi) {
		this.puntosDeVida=puntosDeVida;
		this.puntosDeResistencia=puntosDeResistencia;
		this.puntosAtaqueFisico=puntosAtaqueFisico;
		this.puntosAtaqueKi=puntosAtaqueKi;
	}	
		
	
	//getters & setters	

	public int getPuntosDeVida() {
		return puntosDeVida;
	}

	public void setPuntosDeVida(int puntosDeVida) {
		this.puntosDeVida = puntosDeVida;
	}

	public int getPuntosDeResistencia() {
		return puntosDeResistencia;
	}

	public void setPuntosDeResistencia(int puntosDeResistencia) {
		this.puntosDeResistencia = puntosDeResistencia;
	}

	public int getPuntosAtaqueFisico() {
		return puntosAtaqueFisico;
	}

	public void setPuntosAtaqueFisico(int puntosAtaqueFisico) {
		this.puntosAtaqueFisico = puntosAtaqueFisico;
	}

	public int getPuntosAtaqueKi() {
		return puntosAtaqueKi;
	}

	public void setPuntosAtaqueKi(int puntosAtaqueKi) {
		this.puntosAtaqueKi = puntosAtaqueKi;
	}
}

