package modelos;

import java.util.HashMap;

public abstract class Entidad {
	private static HashMap<Integer, Integer> coordenadasOcupadas;
	private String nombre;
	private String descripcion;
	private int coordenadaX;
	private int coordenadaY;
	

	public Entidad(String nombre, String descripcion, int coordenadaX, int coordenadaY) {
		super();
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.coordenadaX = coordenadaX;
		this.coordenadaY = coordenadaY;
	}


	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public int getCoordenadaX() {
		return coordenadaX;
	}


	public void setCoordenadaX(int coordenadaX) {
		this.coordenadaX = coordenadaX;
	}


	public int getCoordenadaY() {
		return coordenadaY;
	}


	public void setCoordenadaY(int coordenadaY) {
		this.coordenadaY = coordenadaY;
	}


	public String getDescripcion() {
		return descripcion;
	}


	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}


	public static HashMap<Integer, Integer> getCoordenadasOcupadas() {
		return coordenadasOcupadas;
	}


	public static void setCoordenadasOcupadas(HashMap<Integer, Integer> coordenadasOcupadas) {
		Entidad.coordenadasOcupadas = coordenadasOcupadas;
	}
	
}
