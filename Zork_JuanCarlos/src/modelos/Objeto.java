package modelos;

public class Objeto extends Entidad{
	private int moneda;
	private int poder;
	private int valorCompra;
	
	
	public Objeto(String nombre, String descripcion, int coordenadaX, int coordenadaY, int moneda, int poder, int valorCompra) {
		super(nombre, descripcion, coordenadaX, coordenadaY);
		this.moneda = moneda;
		this.poder = poder;
		this.valorCompra = valorCompra;
	}


	public int getMoneda() {
		return moneda;
	}


	public void setMoneda(int moneda) {
		this.moneda = moneda;
	}


	public int getPoder() {
		return poder;
	}


	public void setPoder(int poder) {
		this.poder = poder;
	}


	public int getValorCompra() {
		return valorCompra;
	}


	public void setValorCompra(int valorCompra) {
		this.valorCompra = valorCompra;
	}
	
	
	
}
