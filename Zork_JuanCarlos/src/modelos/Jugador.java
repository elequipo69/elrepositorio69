package modelos;

import java.util.ArrayList;
import java.util.Random;

public class Jugador {
	private int fuerza;
	private ArrayList<Objeto> inventario;
	private int dinero;
	
	
	public Jugador(int fuerza) {
		this.fuerza = fuerza;
	}


	public int getFuerza() {
		return fuerza;
	}


	public void setFuerza(int fuerza) {
		this.fuerza = fuerza;
	}


	public ArrayList<Objeto> getInventario() {
		return inventario;
	}


	public void setInventario(ArrayList<Objeto> inventario) {
		this.inventario = inventario;
	}


	public int getDinero() {
		return dinero;
	}


	public void setDinero(int dinero) {
		this.dinero = dinero;
	}
	
	public int atacar() {
		Random r = new Random();
		
		return r.nextInt(fuerza);
	}
}
