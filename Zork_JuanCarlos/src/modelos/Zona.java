package modelos;

public class Zona {
	private String descripcion;
	private Entidad elemento;
	
	
	public Zona(String descripcion) {
		this.descripcion = descripcion;
	}


	public String getDescripcion() {
		return descripcion;
	}


	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}


	public Entidad getElemento() {
		return elemento;
	}


	public void setElemento(Entidad elemento) {
		this.elemento = elemento;
	}


	@Override
	public String toString() {
		return "Zona [descripcion=" + descripcion + ", elemento=" + elemento + "]";
	}
	
	
}
