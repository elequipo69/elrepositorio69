package modelos;

public class Mapa {
	private Zona[][] casillas;

	public Mapa(Zona[][] casillas) {
		this.casillas = casillas;
	}

	public Zona[][] getCasillas() {
		return casillas;
	}

	public void setCasillas(Zona[][] casillas) {
		this.casillas = casillas;
	}
	
	
}
