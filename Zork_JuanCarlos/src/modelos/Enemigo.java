package modelos;

import java.util.Random;

public class Enemigo extends Entidad{
	private int fuerza;

	public Enemigo(String nombre, String descripcion, int coordenadaX, int coordenadaY, int fuerza) {
		super(nombre, descripcion, coordenadaX, coordenadaY);
		this.fuerza = fuerza;
	}

	public int getFuerza() {
		return fuerza;
	}

	public void setFuerza(int fuerza) {
		this.fuerza = fuerza;
	}
	
	public int atacar() {
		Random r = new Random();
		
		return r.nextInt(fuerza);
	}
}
