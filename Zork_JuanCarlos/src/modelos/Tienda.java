package modelos;

import java.util.ArrayList;

public class Tienda extends Entidad{
	private ArrayList<Objeto> articulo;
	
	
	public Tienda(String nombre, String descripcion, int coordenadaX, int coordenadaY, ArrayList<Objeto> articulo) {
		super(nombre, descripcion, coordenadaX, coordenadaY);
		this.articulo = articulo;
	}


	public ArrayList<Objeto> getArticulo() {
		return articulo;
	}


	public void setArticulo(ArrayList<Objeto> articulo) {
		this.articulo = articulo;
	}


}