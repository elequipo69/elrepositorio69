package loqueyovea;

import excepciones.MuyCortoException;
import excepciones.MuyLargoException;

/*
 * @class Usuario
 * @brief clase que maneja usuario solo con nombre
 * @author Juan Carlos
 * @date 2018-04-11
 */
public class Usuario {
	private String nombre; //Esta variable almacena un nombre entre 3 y 25 caracteres;
	
	/*
	 * @function Usuario (Constructor)
	 * @brief construye un usuario a partir del nombre
	 * @param n String nombre que se le pondr� al usuario
	 * @pre El nombre tiene que tener entre 3 y 25 caracteres
	 */
	public Usuario(String n) throws Exception{
		if(comprobarNombre(n)){
			this.nombre = n;
		}
	}

	public String getNombre() {
		return nombre;
	}
	
	private boolean comprobarNombre(String n) throws Exception{
		if(n.length()<3) {
			//Aqui ya el error
			MuyCortoException e = new MuyCortoException(n.length());
			throw e;
		}
		
		if(n.length()>25) {
			MuyLargoException e = new MuyLargoException(n.length());
			throw e;
		}
		return true;
	}
}
