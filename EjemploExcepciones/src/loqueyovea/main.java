package loqueyovea;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import excepciones.MuyCortoException;
import excepciones.MuyLargoException;

public class main {

	public static void main(String[] args) {
		Usuario u = null;
		boolean nombreValido = false;
		String nombre = "";

		while (nombreValido == false) {
			System.out.println("Introduzca un nombre: ");
			BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
			try {
				nombre = br.readLine();
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}

			try {
				u = new Usuario(nombre);
				System.out.println("he llegado al final del programa");
				nombreValido = true;
			} catch (MuyCortoException mce) {
				System.out.println(mce.muestraMensaje());
			} catch (MuyLargoException mle) {
				System.out.println(mle.muestraMensaje());
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
			if (u != null) {
				System.out.println("Mi nombre es " + u.getNombre());
			}
		}
	}

}
