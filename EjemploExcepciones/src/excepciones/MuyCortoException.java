package excepciones;

public class MuyCortoException extends Exception{
	private String mensaje;
	
	public MuyCortoException(int longitud) {
		super();
		this.mensaje="Tu cadena es demasiado corta, tiene: " + longitud + " caracteres";
	}
	
	public String muestraMensaje() {
		return mensaje;
	}
}
