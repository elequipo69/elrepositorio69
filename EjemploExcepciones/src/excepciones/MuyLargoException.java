package excepciones;

public class MuyLargoException extends Exception{
	private String mensaje;
	
	public MuyLargoException(int longitud) {
		super();
		this.mensaje="Tu cadena es demasiado corta, tiene: " + longitud + " caracteres";
	}
	
	public String muestraMensaje() {
		return mensaje;
	}
}
