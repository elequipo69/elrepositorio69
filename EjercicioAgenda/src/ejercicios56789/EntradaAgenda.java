package ejercicios56789;

public class EntradaAgenda {
	
	private String titulo;
	private String anotacion;
	
	public EntradaAgenda() {
	}
	
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getAnotacion() {
		return anotacion;
	}
	public void setAnotacion(String anotacion) {
		this.anotacion = anotacion;
	}	
}
