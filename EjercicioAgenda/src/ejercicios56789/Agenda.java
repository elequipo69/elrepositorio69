package ejercicios56789;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Scanner;

public class Agenda {

	//Atributos
	private HashMap<LocalDate, EntradaAgenda> agenda = new HashMap<>();
	private int anioAgenda;

	// Constructor
	public Agenda(int anioAgenda) {
		this.anioAgenda = anioAgenda;
	}

	//M�todos

	public void pedirOpciones() {
		Scanner reader = new Scanner(System.in);

		int eleccion;
		do {
			System.out.println("---Elije---");
			System.out.println("1. Insertar entrada ");
			System.out.println("2. Leer una entrada");
			System.out.println("3. Leer toda la agenda");
			System.out.println("4. Salir");
			eleccion = reader.nextInt();
			opciones(eleccion);
		} while (eleccion != 4);
	}

	public void opciones(int eleccion) {
		// Initialize the scanner
		Scanner scanner = new Scanner(System.in);
		
		EntradaAgenda entrada = new EntradaAgenda();

		switch (eleccion) {
		case 1:
			LocalDate fechaAnotacion = pedirFecha();
			System.out.println("Introduce el titulo de tu anotaci�n");
			entrada.setTitulo(scanner.nextLine());
			System.out.println("Ahora introduce tu anotaci�n");
			entrada.setAnotacion(scanner.nextLine());
			if (!agenda.containsKey(fechaAnotacion)) {
				agenda.put(fechaAnotacion, entrada);
				System.out.println("Se ha a�adido tu anotaci�n");
			} else {
				System.out.println("Ya existe una anotaci�n en la fecha " + fechaAnotacion);
			}
			break;
		case 2:
			LocalDate fechaAnotacion1 = pedirFecha();
			mostrarAnotacion(fechaAnotacion1);
			break;
		case 3:
			if (agenda.entrySet().isEmpty()) {
				System.out.println("Tu agenda esta vac�a");
			} else {
				for (Entry<LocalDate, EntradaAgenda> entry : agenda.entrySet()) {
					System.out.println("-----------------------------------------------------------------------------------");
					System.out.println(entry.getKey() + " "
									+ nombreDiaSemana(entry.getKey())
									+ "\nT�tulo:" + entry.getValue().getTitulo()
									+ "\nTexto: " + entry.getValue().getAnotacion());
					System.out.println("-----------------------------------------------------------------------------------");
				}
			}
			break;
		case 4:
			System.out.println("Saliste");
			break;
		default:
			System.out.println("Error, introduce una opci�n v�lida");
			break;
		}
	}

	public LocalDate pedirFecha(){
		Scanner reader = new Scanner(System.in);
		
		System.out.println("Introduce la fecha de tu anotaci�n");
		System.out.print("A�o : ");
		int anio = reader.nextInt();
		System.out.print("Mes : ");
		int mes = reader.nextInt();
		System.out.print("Dia : ");
		int dia = reader.nextInt();
		LocalDate fechaAnotacion = LocalDate.of(anio, mes, dia);
		return fechaAnotacion;
	}
	
	public void mostrarAnotacion(LocalDate fechaAnotacion) {
		System.out.println("-----------------------------------------------------------------------------------");
		if (agenda.containsKey(fechaAnotacion)) {
			System.out.println(fechaAnotacion + " -- "
					+ nombreDiaSemana(fechaAnotacion) + "\nT�tulo:"
					+ agenda.get(fechaAnotacion).getTitulo() + "\nTexto: "
					+ agenda.get(fechaAnotacion).getAnotacion());
		} else {
			System.out.println(fechaAnotacion + " "
					+ nombreDiaSemana(fechaAnotacion) + "\nT�tulo:"
					+ "\nTexto: ");
		}
		System.out.println("-----------------------------------------------------------------------------------");
	}

	public String nombreDiaSemana(LocalDate fechaAnotacion) {
		DayOfWeek nameDayOfWeek = fechaAnotacion.getDayOfWeek();
		String nombreDia = "";
		switch (nameDayOfWeek) {
		case SUNDAY:
			nombreDia = "Domingo";
			break;
		case MONDAY:
			nombreDia = "Lunes";
			break;
		case TUESDAY:
			nombreDia = "Martes";
			break;
		case WEDNESDAY:
			nombreDia = "Mi�rcoles";
			break;
		case THURSDAY:
			nombreDia = "Jueves";
			break;
		case FRIDAY:
			nombreDia = "Viernes";
			break;
		case SATURDAY:
			nombreDia = "Sabado";
			break;
		default:
			break;
		}
		return nombreDia;
	}

	//Getters and setters
	public HashMap<LocalDate, EntradaAgenda> getAgenda() {
		return agenda;
	}

	public void setAgenda(HashMap<LocalDate, EntradaAgenda> agenda) {
		this.agenda = agenda;
	}

	public int getAnioAgenda() {
		return anioAgenda;
	}

	public void setAnioAgenda(int anioAgenda) {
		this.anioAgenda = anioAgenda;
	}
}
