package main;

import java.util.Scanner;
import ejercicios56789.Agenda;

public class Main_Agenda {

	public static void main(String[] args) {
		//Initialize the scanner
		Scanner reader = new Scanner(System.in); 
		
		//Ejercicios 5 y 6

		//Pide el a�o de la agenda
		System.out.print("Introduce el a�o de tu agenda -> ");
		int anio = reader.nextInt();
		Agenda agenda = new Agenda(anio);

		//Pide una de las tres opciones
		agenda.pedirOpciones();		
	}
}
